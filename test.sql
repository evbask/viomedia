-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Дек 05 2018 г., 12:57
-- Версия сервера: 5.6.38
-- Версия PHP: 7.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `test`
--

-- --------------------------------------------------------

--
-- Структура таблицы `nodes`
--

CREATE TABLE `nodes` (
  `id` int(11) NOT NULL,
  `data` varchar(255) NOT NULL,
  `parent_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `nodes`
--

INSERT INTO `nodes` (`id`, `data`, `parent_id`) VALUES
(1, 'Раздел 1', 0),
(2, 'Раздел 2', 0),
(3, 'Раздел 3 тест', 0),
(4, 'Раздел 4', 0),
(5, 'Раздел 5', 0),
(6, 'Подраздел 1.1', 1),
(7, 'Подраздел 1.2', 1),
(8, 'Подраздел 2.1', 2),
(9, 'Подраздел 2.1.1', 8),
(10, 'Подраздел 2.1.2', 8),
(11, 'Подраздел 2.1.3', 8),
(12, 'Подраздел 4.1', 4),
(13, 'Подраздел 4.2', 4),
(14, 'Подраздел 4.2.1', 13),
(15, 'Подраздел 2.1.3.1', 11);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `storage` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `storage`) VALUES
(6, 'a:3:{s:3:\"foo\";s:3:\"bar\";s:1:\"g\";i:24;s:5:\"multi\";a:1:{s:11:\"dimensional\";a:1:{s:5:\"array\";s:3:\"foo\";}}}'),
(8, 'a:4:{s:4:\"name\";s:7:\"Evgeniy\";s:3:\"age\";i:22;s:4:\"home\";a:4:{s:4:\"city\";s:11:\"Arkhangelsk\";s:5:\"state\";s:17:\"State of New York\";s:8:\"location\";a:2:{s:3:\"lat\";d:43;s:4:\"long\";d:30.449999999999999;}s:8:\"district\";s:8:\"downtown\";}s:4:\"work\";a:3:{s:4:\"main\";a:2:{s:4:\"role\";s:8:\"Director\";s:7:\"address\";s:19:\"New York, 56 street\";}s:10:\"additional\";a:2:{s:4:\"role\";s:6:\"Driver\";s:7:\"address\";N;}s:3:\"new\";a:2:{s:4:\"role\";s:7:\"Nothing\";s:7:\"address\";s:19:\"New York, 58 street\";}}}'),
(11, 'a:4:{s:4:\"name\";s:7:\"Evgeniy\";s:3:\"age\";i:68;s:4:\"home\";a:3:{s:4:\"city\";s:8:\"New York\";s:5:\"state\";s:17:\"State of New York\";s:8:\"location\";a:2:{s:3:\"lat\";i:66;s:4:\"long\";i:77;}}s:4:\"work\";a:3:{s:4:\"main\";a:2:{s:4:\"role\";s:8:\"Director\";s:7:\"address\";s:19:\"New York, 56 street\";}s:10:\"additional\";a:2:{s:4:\"role\";s:6:\"Driver\";s:7:\"address\";N;}s:3:\"new\";a:2:{s:4:\"role\";s:3:\"CEO\";s:7:\"address\";s:8:\"downtown\";}}}'),
(12, 'a:4:{s:4:\"name\";s:4:\"Ivan\";s:3:\"age\";i:68;s:4:\"home\";a:3:{s:4:\"city\";s:8:\"New York\";s:5:\"state\";s:17:\"State of New York\";s:8:\"location\";a:2:{s:3:\"lat\";d:43;s:4:\"long\";d:30.449999999999999;}}s:4:\"work\";a:2:{s:4:\"main\";a:2:{s:4:\"role\";s:8:\"Director\";s:7:\"address\";s:19:\"New York, 56 street\";}s:10:\"additional\";a:2:{s:4:\"role\";s:6:\"Driver\";s:7:\"address\";N;}}}');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `nodes`
--
ALTER TABLE `nodes`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `nodes`
--
ALTER TABLE `nodes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
