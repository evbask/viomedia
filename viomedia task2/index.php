<?php
require_once 'connection.php';
require_once 'User.php';
require_once 'functions.php';

/* STORAGE, добавленные в бд
$array = array(                      id = 12;
    "name" => "Ivan",
    "age"    => 68,
    "home" => array(
        "city" => "New York",
        "state" => "State of New York",
        "location" => array(
            "lat" => 43.00,
            "long" => 30.45
        )
    ),
    "work" => array(
        "main" => array(
            "role" => "Director",
            "address" => "New York, 56 street"
        ),
        "additional" => array(
            "role" => "Driver",
            "address" => null
        )
    )
);

$array2 = array(
    "foo" => "bar",
    "g" => 24,
    "multi" => array(
        "dimensional" => array(
            "array"=>"foo"
    ),
    ),
);
*/

//$mysqli = dbConnect(HOST,USER, PASS, DATABASE);
//insertUser($mysqli,$array); //вставка нового пользователя в базу

$user = User::getInstance(12);  // Получение пользователя по id
$name = $user->get("name"); //return Ivan
echo $name.'</br>';
$location = $user->get("home/location"); //return array("lat" => 43.00, "long" => 30.45)
echo $location["lat"].'</br>'; // return 43
$long = $user->get("home/location/long"); // return 30.45
echo $long.'</br>';
$role = $user->get("work/main/role");  // return Director
echo $role.'</br>';

$user->set("name","Evgeniy"); // в бд и storage класса имя изменится
$user->set("home/location",array("lat"=>66,"long"=>77)); //в бд и storage класса координаты изменятся
$user->set("work/new",array("role"=>"CEO","address"=>"downtown")); //добавляем новую работу в бд и storage

echo $user->get("work/new/role")." ".$user->get("work/new/address"); // return CEO и return downtown


?>