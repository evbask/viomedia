<?php

/**
 * @param $host
 * @param $user
 * @param $password
 * @param $db
 *
 * @return Exception|mysqli
 */
function dbConnect($host, $user, $password, $db) {
    try {
      $db = mysqli_connect($host, $user, $password, $db);

           if(!$db){
               throw new Exception('Unable to connect');
           }else{
               return $db;
           }
    } catch (Exception $e) {
        echo $e->getMessage();
        return $e;
    }
}

?>