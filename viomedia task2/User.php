<?php
require_once 'config.php';
require_once 'connection.php';

class User
{
    private static $instance = null;
    private static $storage;
    private static $con;
    private static $id;


    /**
     * @param $id
     *
     * @return null|User
     */
    public static function getInstance($id)
    {
        if (null === self::$instance) {
            self::$instance = new self();
            self::$id = $id;

            self::$storage = self::getStorage();
        }
        return self::$instance;
    }

    private function __clone()
    {
    }

    private function __construct()
    {
    }

    public function dump()
    {
        var_dump($this);
    }

    /**
     * @param $path
     *
     * @return mixed
     */
    public function get($path)         //геттер
    {
        $keys = explode("/", $path); //помещаем путь в массив, например для example/task $keys = ["example","task"]
        $item =& self::$storage;
        foreach ($keys as $key) {
            $item =& $item[$key];   //проходимся по переданному пути,в $item передается значение по ссылке, с каждой итерацией меняется указатель
        }

        return $item;
    }

    /**
     * @param $path
     * @param $newValue
     *
     * @return Exception|string
     */
    public function set($path, $newValue) //сеттер
    {
        $keys = explode("/", $path);
        $item =& self::$storage;
        foreach ($keys as $key) {
            $item =& $item[$key];
        }
        $item = $newValue; //после цикла $item указывает на нужное место в storage, присваиваем ей новое значение. $item указывает на хранилище, поэтому при ёё изменении изменяется и хранилище.
        $id = self::$id;
        $data = self::serializator(self::$storage);//сериализуем хранилище
        try {
            if ($result = mysqli_query(self::$con,
                "UPDATE users SET storage='$data' WHERE id='$id'") //обновляем бд
            ) {
                return "success";
            } else {
                throw new Exception('Unable to set new value');
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            return $e;
        }

    }

    /**
     * @return Exception|mixed
     */
    private function getStorage()//функция, возвращающая десериализованное хранилище
    {
        self::$con = dbConnect(HOST, USER, PASS, DATABASE);
        $id = self::$id;
        try {
            if ($result = mysqli_query(self::$con,
                "SELECT * FROM users WHERE id='$id'")
            ) {
                return self::unserializator(mysqli_fetch_array($result,
                    MYSQLI_ASSOC)["storage"]);
            } else {
                throw new Exception('Unable to get storage');
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            return $e;
        }
    }

    /**
     * @param $array
     *
     * @return string
     */
    public function serializator($array)
    {
        return serialize($array);
    }

    /**
     * @param $array
     *
     * @return mixed
     */
    public function unserializator($array)
    {
        return unserialize($array);
    }


}

?>