<?php
require_once 'NodesManager.php';
require_once 'render.php';

/*
 Древовидная структура в БД:
Раздел 1 [id=1][parent_id=0]
    Подраздел 1.1 [id=6][parent_id=1]
    Подраздел 1.2 [id=7][parent_id=1]
Раздел 2 [id=2][parent_id=0]
    Подраздел 2.1 [id=8][parent_id=2]
        Подраздел 2.1.1 [id=9][parent_id=8]
        Подраздел 2.1.2 [id=10][parent_id=8]
        Подраздел 2.1.3 [id=11][parent_id=8]
            Подраздел 2.1.3.1 [id=15][parent_id=11]
Раздел 3 тест [id=3][parent_id=0]
Раздел 4 [id=4][parent_id=0]
    Подраздел 4.1 [id=12][parent_id=4]
    Подраздел 4.2 [id=13][parent_id=4]
        Подраздел 4.2.1 [id=14][parent_id=13]
Раздел 5 [id=5][parent_id=0]
 */

$nodes = NodesManager::getSubNodes(2); //функция, которая извлекает все вложенные узлы, принадлежащие узлу с указанным id.
// При id = 0  на экран выведутся все узлы
echo renderTemplate('template.php',['nodes'=>$nodes]); //подключаем шаблонизатор для упорядоченного вывода
// древовидной структуры нужного Node на экарн
/* При данном id=2 выведет
Раздел 2 [id=2][parent_id=0]
    Подраздел 2.1 [id=8][parent_id=2]
        Подраздел 2.1.1 [id=9][parent_id=8]
        Подраздел 2.1.2 [id=10][parent_id=8]
        Подраздел 2.1.3 [id=11][parent_id=8]
            Подраздел 2.1.3.1 [id=15][parent_id=11]
*/

?>