<ul style="margin-left:10px;">

    <?php foreach($nodes as $node) : ?>
        <li><?php echo $node['data']." [id=".$node['id']."]"; ?></li>
        <?php if(count($node['children']) > 0) : ?>
            <?php echo renderTemplate('node.php',['nodes'=>$node['children']]); ?>
        <?php  endif; ?>

    <?php endforeach; ?>

</ul>