<?php
require_once 'connection.php';
require_once 'config.php';

abstract class NodesManager
{
    /**
     * @param $id
     *
     * @return array|mixed|null
     */
    public static function getSubNodes($id)
    {   //функция, получающая все вложенные узлы, принадлежащие узлу с указанным $id

        $mysqli = dbConnect(HOST, USER, PASS, DATABASE);
        $nodes = self::getAllNodes($mysqli);
        $nodes = self::initTree($nodes, $id);
        return $nodes;

    }

    /**
     * @param $db
     * @param $id
     * @param $data
     *
     * @return bool|Exception|mysqli_result
     */
    public static function updateNode($db, $id, $data)
    { //обновляет значение в узле
        try {
            $result = mysqli_query($db,
                "UPDATE nodes SET data='$data' WHERE id='$id'");
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            return $e;
        }
    }

    /**
     * @param $db
     * @param $data
     * @param $parent_id
     *
     * @return Exception|string
     */
    public static function insertNode($db,$data,$parent_id) { //добавляет новый узел

        try{
            if ($result = mysqli_query($db,"INSERT INTO nodes (data,parent_id) VALUES ('$data','$parent_id')")) {
                return "success";
            }else{
                throw new Exception('Unable to insert node');
            }
        }catch (Exception $e){
            echo $e->getMessage();
            return $e;
        }

    }
    /**
     * @param $db
     *
     * @return array|Exception|null
     */
    private static function getAllNodes($db)
    { //функция, получающая все узлы
        try {
            if ($result = mysqli_query($db, "SELECT * FROM nodes")) {
                return mysqli_fetch_all($result, MYSQLI_ASSOC);
            } else {
                throw new Exception('Unable to get nodes');
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            return $e;
        }
    }

    /**
     * @param $arr
     * @param $id
     *
     * @return mixed
     */
    private static function initTree($arr, $id)
    {
        $parents = array();
        $main_node = '';
        foreach ($arr as $key => $item) {
            $parents[$item['parent_id']][$item['id']]
                = $item; //формируем массив, в котором ключами являются id родителей
            //в каждой ячейке массива содержится подмассив дочерних элементов
            if ($id == $item['id']) {
                $main_node = $item;
            }
        }

        ($id > 0)
            ? $tree[$id] = $main_node
            : $tree
            = $parents[$id];//в $tree сохраняется одна из ячеек массива parents в завимимости от того, какой id был передан
        self::generateNodeTree($tree, $parents);

        return $tree;
    }

    /**
     * @param $treeElem
     * @param $parents_arr
     */
    private static function generateNodeTree(&$tree, $parents){ //функция, рекурсивно формирующая древовидную структуру нужного нам Node
        foreach ($tree as $key => $item) {
            if (!isset($item['children'])) {
                $tree[$key]['children'] = array();
            }
            if (array_key_exists($key,$parents)) { //если у очередного элемента есть потомки
                $tree[$key]['children'] = $parents[$key]; //в поле children записываем с ними массив
                self::generateNodeTree($tree[$key]['children'],$parents);//потом прогоняем этот массив через эту же функцию
            }
        }
    }

}


?>